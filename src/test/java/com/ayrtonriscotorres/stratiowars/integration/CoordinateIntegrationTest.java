package com.ayrtonriscotorres.stratiowars.integration;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.bind.MethodArgumentNotValidException;

import com.ayrtonriscotorres.stratiowars.controller.constant.Endpoint;
import com.ayrtonriscotorres.stratiowars.util.TestUtils;

@SpringBootTest
@AutoConfigureMockMvc
public class CoordinateIntegrationTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@Test
	public void decryptCoordinatesTest() throws Exception {
		// @formatter:off
		mockMvc.perform(
				post(Endpoint.COORDINATE + Endpoint.DECRYPT)
					.contentType(MediaType.APPLICATION_JSON)
					.content(TestUtils.COORDINATES_REQUEST_BODY))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("coordinates", is(notNullValue())))
				.andExpect(jsonPath("coordinates[0].uuid").value(TestUtils.UUID_0))
				.andExpect(jsonPath("coordinates[0].decrypted").value(TestUtils.UUID_0_DECRYPTED))
				.andExpect(jsonPath("coordinates[1].uuid").value(TestUtils.UUID_1))
				.andExpect(jsonPath("coordinates[1].decrypted").value(TestUtils.UUID_1_DECRYPTED));
		// @formatter:on
	}
	
	
	@Test
	public void decryptCoordinatesMalformedUuidTest() throws Exception {
		// @formatter:off
		mockMvc.perform(
				post(Endpoint.COORDINATE + Endpoint.DECRYPT)
					.contentType(MediaType.APPLICATION_JSON)
					.content(TestUtils.COORDINATES_ERROR_REQUEST_BODY))
				.andDo(print())
				.andExpect(status().isBadRequest())
				.andExpect(result -> assertTrue(result.getResolvedException() instanceof MethodArgumentNotValidException));
		// @formatter:on
	}
}
