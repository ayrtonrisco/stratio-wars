package com.ayrtonriscotorres.stratiowars.util;

public final class TestUtils {
	
	private TestUtils() {}
	
	public static final String GALAXY_0 = "6f9c15fa";
	public static final String QUADRANT_0 = "ef51";
	public static final String STARSYSTEM1_0 = "4415";
	public static final String STARSYSTEM2_0 = "afab";
	public static final String PLANET_0 = "36218d76c2d9";
	
	public static final String GALAXY_0_DECRYPTED = "73";
	public static final String QUADRANT_0_DECRYPTED = "15";
	public static final String STARSYSTEM_0_DECRYPTED = "46";
	public static final String PLANET_0_DECRYPTED = "dc9876321";
	
	public static final String GALAXY_1 = "2ab81c9b";
	public static final String QUADRANT_1 = "1719";
	public static final String STARSYSTEM1_1 = "400c";
	public static final String STARSYSTEM2_1 = "a676";
	public static final String PLANET_1 = "bdba976150eb";
	
	public static final String GALAXY_1_DECRYPTED = "64";
	public static final String QUADRANT_1_DECRYPTED = "9";
	public static final String STARSYSTEM_1_DECRYPTED = "35";
	public static final String PLANET_1_DECRYPTED = "edba976510";
	
	public static final String UUID_0 = GALAXY_0 + "-" + QUADRANT_0 + "-" + STARSYSTEM1_0 + "-" + STARSYSTEM2_0 + "-" + PLANET_0;
	public static final String UUID_1 = GALAXY_1 + "-" + QUADRANT_1 + "-" + STARSYSTEM1_1 + "-" + STARSYSTEM2_1 + "-" + PLANET_1;
	
	public static final String UUID_0_DECRYPTED = GALAXY_0_DECRYPTED + "-" + QUADRANT_0_DECRYPTED + "-" + STARSYSTEM_0_DECRYPTED + "-" + PLANET_0_DECRYPTED;
	public static final String UUID_1_DECRYPTED = GALAXY_1_DECRYPTED + "-" + QUADRANT_1_DECRYPTED + "-" + STARSYSTEM_1_DECRYPTED + "-" + PLANET_1_DECRYPTED;
	
	public static final String INVALID_UUID = "1234567890";
			
	
	// @formatter:off
	public static final String COORDINATES_REQUEST_BODY =
	"{" +
	"    \"coordinates\": [" +
	"        {" +
	"            \"uuid\": \"" + UUID_0 + "\"" +
	"        }," +
	"        {" +
	"            \"uuid\": \"" + UUID_1 + "\"" +
	"        }" +
	"    ]" +
	"}";
	// @formatter:on
	
	// @formatter:off
	public static final String COORDINATES_ERROR_REQUEST_BODY =
	"{" +
	"    \"coordinates\": [" +
	"        {" +
	"            \"uuid\": \"" + UUID_0 + "\"" +
	"        }," +
	"        {" +
	"            \"uuid\": \"" + INVALID_UUID + "\"" +
	"        }" +
	"    ]" +
	"}";
	// @formatter:on
}
