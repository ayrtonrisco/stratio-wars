package com.ayrtonriscotorres.stratiowars.service;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.ayrtonriscotorres.stratiowars.domain.Coordinate;
import com.ayrtonriscotorres.stratiowars.util.TestUtils;

@SpringBootTest
public class CoordinateServiceTest {

	@Autowired
	private CoordinateService coordinateService;
	
	@Test
	public void decryptCoordinateZeroTest() {
		Coordinate request = buildEncryptedCoordinateZero();
		
        Coordinate actual = coordinateService.decrypt(request);
        
        assertThat(actual.getGalaxy()).isEqualTo(TestUtils.GALAXY_0_DECRYPTED);
        assertThat(actual.getQuadrant()).isEqualTo(TestUtils.QUADRANT_0_DECRYPTED);
        assertThat(actual.getStarSystem()).isEqualTo(TestUtils.STARSYSTEM_0_DECRYPTED);
        assertThat(actual.getPlanet()).isEqualTo(TestUtils.PLANET_0_DECRYPTED);
	}
	
	@Test
	public void decryptCoordinateOneTest() {
		Coordinate request = buildEncryptedCoordinateOne();
		
        Coordinate actual = coordinateService.decrypt(request);
        
        assertThat(actual.getGalaxy()).isEqualTo(TestUtils.GALAXY_1_DECRYPTED);
        assertThat(actual.getQuadrant()).isEqualTo(TestUtils.QUADRANT_1_DECRYPTED);
        assertThat(actual.getStarSystem()).isEqualTo(TestUtils.STARSYSTEM_1_DECRYPTED);
        assertThat(actual.getPlanet()).isEqualTo(TestUtils.PLANET_1_DECRYPTED);
	}

	private Coordinate buildEncryptedCoordinateZero() {
		// @formatter:off
		return Coordinate.builder()
				.withEncryptedGalaxy(TestUtils.GALAXY_0)
				.withEncryptedQuadrant(TestUtils.QUADRANT_0)
				.withEncryptedStarSystem1(TestUtils.STARSYSTEM1_0)
				.withEncryptedStarSystem2(TestUtils.STARSYSTEM2_0)
				.withEncryptedPlanet(TestUtils.PLANET_0)
				.build();
		// @formatter:on
	}
	
	private Coordinate buildEncryptedCoordinateOne() {
		// @formatter:off
		return Coordinate.builder()
				.withEncryptedGalaxy(TestUtils.GALAXY_1)
				.withEncryptedQuadrant(TestUtils.QUADRANT_1)
				.withEncryptedStarSystem1(TestUtils.STARSYSTEM1_1)
				.withEncryptedStarSystem2(TestUtils.STARSYSTEM2_1)
				.withEncryptedPlanet(TestUtils.PLANET_1)
				.build();
		// @formatter:on
	}
}
