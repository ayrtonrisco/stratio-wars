package com.ayrtonriscotorres.stratiowars.controller.dto;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;


public class CoordinateWrapperDTO {

	@Valid
	@NotNull
	private List<CoordinateDTO> coordinates;

	public List<CoordinateDTO> getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(List<CoordinateDTO> coordinates) {
		this.coordinates = coordinates;
	}

}
