package com.ayrtonriscotorres.stratiowars.controller.dto;

import javax.validation.constraints.Pattern;

public class CoordinateDTO {

	@Pattern(regexp = "([a-f0-9]{8}(-[a-f0-9]{4}){4}[a-f0-9]{8})")
	private String uuid;
	private String decrypted;

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getDecrypted() {
		return decrypted;
	}

	public void setDecrypted(String decrypted) {
		this.decrypted = decrypted;
	}

}
