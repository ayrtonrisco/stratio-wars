package com.ayrtonriscotorres.stratiowars.controller.constant;

public final class Endpoint {

	private Endpoint() {}

	public static final String COORDINATE = "/coordinate";
	public static final String DECRYPT = "/decrypt";

}
