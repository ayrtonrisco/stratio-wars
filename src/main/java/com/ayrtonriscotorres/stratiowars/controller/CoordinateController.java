package com.ayrtonriscotorres.stratiowars.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ayrtonriscotorres.stratiowars.controller.constant.Endpoint;
import com.ayrtonriscotorres.stratiowars.controller.dto.CoordinateDTO;
import com.ayrtonriscotorres.stratiowars.controller.dto.CoordinateWrapperDTO;
import com.ayrtonriscotorres.stratiowars.domain.Coordinate;
import com.ayrtonriscotorres.stratiowars.service.CoordinateService;

@RestController
@RequestMapping(path = Endpoint.COORDINATE)
public class CoordinateController {

	@Autowired
	private CoordinateService coordinateService;

	@PostMapping(path = Endpoint.DECRYPT)
	public CoordinateWrapperDTO decryptCoordinates(@RequestBody @Valid CoordinateWrapperDTO wrapper) {
		// @formatter:off
		List<CoordinateDTO> decryptedCoordinates = wrapper.getCoordinates()
				.stream()
				.map(this::mapToDomain)
				.map(coordinateService::decrypt)
				.map(this::mapToDTO)
				.collect(Collectors.toList());
		// @formatter:on

		CoordinateWrapperDTO response = new CoordinateWrapperDTO();
		response.setCoordinates(decryptedCoordinates);
		return response;
	}

	private Coordinate mapToDomain(CoordinateDTO dto) {
		// @formatter:off
		return Coordinate.builder()
		.withEncryptedGalaxy(getGroupByPositionUuid(dto.getUuid(), 0))
		.withEncryptedQuadrant(getGroupByPositionUuid(dto.getUuid(), 1))
		.withEncryptedStarSystem1(getGroupByPositionUuid(dto.getUuid(), 2))
		.withEncryptedStarSystem2(getGroupByPositionUuid(dto.getUuid(), 3))
		.withEncryptedPlanet(getGroupByPositionUuid(dto.getUuid(), 4))
		.build();
		// @formatter:on
	}

	private CoordinateDTO mapToDTO(Coordinate domain) {
		CoordinateDTO dto = new CoordinateDTO();
		dto.setUuid(domain.getEncryptedString());
		dto.setDecrypted(domain.getDecryptedString());
		return dto;
	}

	private String getGroupByPositionUuid(String str, int position) {
		return str.split("-")[position];
	}

}
