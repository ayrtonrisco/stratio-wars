package com.ayrtonriscotorres.stratiowars;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StratiowarsApplication {

	public static void main(String[] args) {
		SpringApplication.run(StratiowarsApplication.class, args);
	}

}
