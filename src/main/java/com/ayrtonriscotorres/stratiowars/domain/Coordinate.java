package com.ayrtonriscotorres.stratiowars.domain;

public class Coordinate {

	private final String encryptedGalaxy;
	private final String encryptedQuadrant;
	private final String encryptedStarSystem1;
	private final String encryptedStarSystem2;
	private final String encryptedPlanet;

	private final String galaxy;
	private final String quadrant;
	private final String starSystem;
	private final String planet;

	public String getEncryptedGalaxy() {
		return encryptedGalaxy;
	}

	public String getEncryptedQuadrant() {
		return encryptedQuadrant;
	}

	public String getEncryptedStarSystem1() {
		return encryptedStarSystem1;
	}

	public String getEncryptedStarSystem2() {
		return encryptedStarSystem2;
	}

	public String getEncryptedPlanet() {
		return encryptedPlanet;
	}

	public String getGalaxy() {
		return galaxy;
	}

	public String getQuadrant() {
		return quadrant;
	}

	public String getStarSystem() {
		return starSystem;
	}

	public String getPlanet() {
		return planet;
	}

	private Coordinate(Builder builder) {
		this.encryptedGalaxy = builder.encryptedGalaxy;
		this.encryptedQuadrant = builder.encryptedQuadrant;
		this.encryptedStarSystem1 = builder.encryptedStarSystem1;
		this.encryptedStarSystem2 = builder.encryptedStarSystem2;
		this.encryptedPlanet = builder.encryptedPlanet;
		this.galaxy = builder.galaxy;
		this.quadrant = builder.quadrant;
		this.starSystem = builder.starSystem;
		this.planet = builder.planet;
	}

	public static IEncryptedGalaxyStage builder() {
		return new Builder();
	}

	public interface IEncryptedGalaxyStage {
		public IEncryptedQuadrantStage withEncryptedGalaxy(String encryptedGalaxy);
	}

	public interface IEncryptedQuadrantStage {
		public IEncryptedStarSystem1Stage withEncryptedQuadrant(String encryptedQuadrant);
	}

	public interface IEncryptedStarSystem1Stage {
		public IEncryptedStarSystem2Stage withEncryptedStarSystem1(String encryptedStarSystem1);
	}

	public interface IEncryptedStarSystem2Stage {
		public IEncryptedPlanetStage withEncryptedStarSystem2(String encryptedStarSystem2);
	}

	public interface IEncryptedPlanetStage {
		public IBuildStage withEncryptedPlanet(String encryptedPlanet);
	}

	public interface IBuildStage {
		public IBuildStage withGalaxy(String galaxy);

		public IBuildStage withQuadrant(String quadrant);

		public IBuildStage withStarSystem(String starSystem);

		public IBuildStage withPlanet(String planet);

		public Coordinate build();
	}

	public static final class Builder implements IEncryptedGalaxyStage, IEncryptedQuadrantStage,
			IEncryptedStarSystem1Stage, IEncryptedStarSystem2Stage, IEncryptedPlanetStage, IBuildStage {
		private String encryptedGalaxy;
		private String encryptedQuadrant;
		private String encryptedStarSystem1;
		private String encryptedStarSystem2;
		private String encryptedPlanet;
		private String galaxy;
		private String quadrant;
		private String starSystem;
		private String planet;

		private Builder() {
		}

		@Override
		public IEncryptedQuadrantStage withEncryptedGalaxy(String encryptedGalaxy) {
			this.encryptedGalaxy = encryptedGalaxy;
			return this;
		}

		@Override
		public IEncryptedStarSystem1Stage withEncryptedQuadrant(String encryptedQuadrant) {
			this.encryptedQuadrant = encryptedQuadrant;
			return this;
		}

		@Override
		public IEncryptedStarSystem2Stage withEncryptedStarSystem1(String encryptedStarSystem1) {
			this.encryptedStarSystem1 = encryptedStarSystem1;
			return this;
		}

		@Override
		public IEncryptedPlanetStage withEncryptedStarSystem2(String encryptedStarSystem2) {
			this.encryptedStarSystem2 = encryptedStarSystem2;
			return this;
		}

		@Override
		public IBuildStage withEncryptedPlanet(String encryptedPlanet) {
			this.encryptedPlanet = encryptedPlanet;
			return this;
		}

		@Override
		public IBuildStage withGalaxy(String galaxy) {
			this.galaxy = galaxy;
			return this;
		}

		@Override
		public IBuildStage withQuadrant(String quadrant) {
			this.quadrant = quadrant;
			return this;
		}

		@Override
		public IBuildStage withStarSystem(String starSystem) {
			this.starSystem = starSystem;
			return this;
		}

		@Override
		public IBuildStage withPlanet(String planet) {
			this.planet = planet;
			return this;
		}

		@Override
		public Coordinate build() {
			return new Coordinate(this);
		}
	}

	public String getEncryptedString() {
		return encryptedGalaxy + "-" + encryptedQuadrant + "-" + encryptedStarSystem1 + "-" + encryptedStarSystem2 + "-"
				+ encryptedPlanet;
	}

	public String getDecryptedString() {
		return galaxy + "-" + quadrant + "-" + starSystem + "-" + planet;
	}

}
