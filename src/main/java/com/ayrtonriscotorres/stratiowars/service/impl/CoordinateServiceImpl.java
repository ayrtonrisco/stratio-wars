package com.ayrtonriscotorres.stratiowars.service.impl;

import java.util.Comparator;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.springframework.stereotype.Service;

import com.ayrtonriscotorres.stratiowars.domain.Coordinate;
import com.ayrtonriscotorres.stratiowars.service.CoordinateService;

@Service
public class CoordinateServiceImpl implements CoordinateService {

	@Override
	public Coordinate decrypt(Coordinate coordinate) {
		String galaxy = decryptGalaxy(coordinate.getEncryptedGalaxy());
		String quadrant = decryptQuadrant(coordinate.getEncryptedQuadrant());
		String starSystem = decryptStarSystem(coordinate.getEncryptedStarSystem1(), coordinate.getEncryptedStarSystem2());
		String planet = decryptPlanet(coordinate.getEncryptedPlanet());

		// @formatter:off
		return Coordinate.builder()
				.withEncryptedGalaxy(coordinate.getEncryptedGalaxy())
				.withEncryptedQuadrant(coordinate.getEncryptedQuadrant())
				.withEncryptedStarSystem1(coordinate.getEncryptedStarSystem1())
				.withEncryptedStarSystem2(coordinate.getEncryptedStarSystem2())
				.withEncryptedPlanet(coordinate.getEncryptedPlanet())
				.withGalaxy(galaxy)
				.withQuadrant(quadrant)
				.withStarSystem(starSystem)
				.withPlanet(planet)
				.build();
		// @formatter:on
	}

	private String decryptGalaxy(String encryptedGalaxy) {
		return String.valueOf(sumHexValueOfEachChar(encryptedGalaxy));
	}

	private String decryptQuadrant(String encryptedQuadrant) {
		return String.valueOf(maxHexValue(encryptedQuadrant));
	}
	
	private String decryptStarSystem(String encryptedStarSystem1, String encryptedStarSystem2) {
		return String.valueOf(compareAndSumHexValues(encryptedStarSystem1.toCharArray(), encryptedStarSystem2.toCharArray()));
	}
	
	private String decryptPlanet(String encryptedPlanet) {
		return filterRepeatedCharsAndReverseOrder(encryptedPlanet);
	}

	private int sumHexValueOfEachChar(String str) {
		// @formatter:off
		return str.chars()
				.mapToObj(character -> (char) character)
				.mapToInt(this::parseHex)
				.sum();
		// @formatter:on
	}
	
	private int maxHexValue(String str) {
		// @formatter:off
		return str.chars()
				.mapToObj(character -> (char) character)
				.mapToInt(this::parseHex)
				.max()
				.getAsInt();
		// @formatter:on
	}
	
	private int compareAndSumHexValues(char[] str1, char[] str2) {
		// @formatter:off
		return IntStream
		  .range(0, Math.min(str1.length, str2.length))
		  .map(i -> Math.max(parseHex(str1[i]), parseHex(str2[i])))
		  .sum();
		// @formatter:on
	}

	private String filterRepeatedCharsAndReverseOrder(String str) {
		// @formatter:off
		return str.chars()
				.distinct()
				.sorted()
				.mapToObj(c -> String.valueOf((char) c))
				.sorted(Comparator.reverseOrder())
				.collect(Collectors.joining());
		// @formatter:on
	}


	private int parseHex(char c) {
		return Integer.decode("0x" + c);
	}
}
