package com.ayrtonriscotorres.stratiowars.service;

import com.ayrtonriscotorres.stratiowars.domain.Coordinate;

public interface CoordinateService {
	Coordinate decrypt(Coordinate coordinate);
}
