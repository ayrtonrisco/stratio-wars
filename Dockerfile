FROM maven:3.8.1-openjdk-11

# copy all to the container
COPY ./ ./
 
RUN mvn clean package

CMD ["java", "-jar", "target/stratiowars-0.0.1-SNAPSHOT.jar"]

EXPOSE 8080