# Stratio Wars - Ayrton Rafael Risco Torres
## Deploy
### Docker
This project has a Dockerfile that compiles and deploys the project. Therefore it is only necessary build the image and run it.
#### Build image
> docker build -t stratio-wars:latest .

#### Run
> docker run -dp 8080:8080 stratio-wars

### Manual

We can also compile and deploy manually. You need:

* Java 11+
* Maven 3+

#### Compile

From the root of the project, we clean and package the project.
> mvn clean package

#### Deploy
Execute the jar that deploys the application.
> java -jar target\stratiowars-0.0.1-SNAPSHOT.jar

## Endpoint
> POST -> http://{ROOT_PATH}:8080/coordinate/decrypt

Body of the request and response
```json
{
  "coordinates": [
    {
        "uuid": "string",
        "decrypted": "string"
    }
  ]
}
```

#### Example request:
```json
{
    "coordinates": [
        {
            "uuid": "6f9c15fa-ef51-4415-afab-36218d76c2d9"
        },
        {
            "uuid": "2ab81c9b-1719-400c-a676-bdba976150eb"
        }
    ]
}
```
#### Example response:
```json
{
    "coordinates": [
        {
            "uuid": "6f9c15fa-ef51-4415-afab-36218d76c2d9",
            "decrypted": "73-15-46-dc9876321"
        },
        {
            "uuid": "2ab81c9b-1719-400c-a676-bdba976150eb",
            "decrypted": "64-9-35-edba976510"
        }
    ]
}
```